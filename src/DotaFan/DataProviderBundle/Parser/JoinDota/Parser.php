<?php

namespace DotaFan\DataProviderBundle\Parser\JoinDota;

use DotaFan\DataProviderBundle\Parser\JoinDota\Exception\GameNotFoundException;
use DotaFan\MainBundle\Service\HTTP;

class Parser
{
	private $baseUri = 'http://www.joindota.com';
	private $http;

	public function __construct()
	{
		$this->http = new HTTP();
	}

	private function getMatchesUri()
	{
		return $this->baseUri . '/en/matches';
	}

	private function getHomeUri()
	{
		return $this->baseUri . '/en/start';
	}

	/**
	 * @return \ArrayObject<array>
	 */
	public function getGames($page = 1)
	{
		$page = (int) $page;
		if ($page <= 1) {
			$page = 1;
		}

		$uri = $this->getMatchesUri().'/&archiv_page='.$page;
		list($html, $uri) = $this->http->loadUri($uri);
		if ($html !== false) {

			$matchList = new \simple_html_dom();
			$matchList->load($html);

			$ret = new \ArrayObject();

			foreach ($matchList->find('div#content .item') as $domGame) {
				$uri = $domGame->find('a', 0)->href;
				$game = [
					'uri' => $uri
				];
				$ret->append($game);
			}

			$matchList->clear();
			unset($matchList);

			return $ret;

		} else {
			throw new \Exception('Could not load HTML '.$uri);
		}
	}

	public function getGame($uri)
	{
		list($html, $new_uri) = $this->http->loadUri($uri);

		if ($new_uri == $this->getHomeUri()) {
			$e = new GameNotFoundException();
			$e->setUri($uri);
			throw $e;
		} else {
			$uri = $new_uri;
		}

		if ($html !== false) {
			$dom = new \simple_html_dom();
			$dom->load($html);

			$ret = [
				'uri' => $uri
			];

			// tournament
			$ret['tournament'] = [
				'name' => trim($dom->find('.match_head .left a', 0)->plaintext),
				'uri' => $dom->find('.match_head .left a', 0)->href,
			];
			$ret['bracket'] = trim($dom->find('.match_head .left text', -1)->plaintext, '- ');
			if ($ret['tournament']['name'] === $ret['bracket']) {
				$ret['bracket'] = '';
			}
			$ret['fullDate'] = trim($dom->find('.match_head .right', 0)->plaintext);

			$status = trim(strip_tags($dom->find('.match_logos .score', 0)->plaintext));
			$status = strip_tags($status);
			$status = strtok($status, "\n");
			$status = trim($status);

			$format = ($el = $dom->find('.match_logos .score span[style="font-size: 14px;"]', 0)) ? $el->plaintext : '';
			$format = trim($format);

			if ($format == $status) {
				$status = '';
			} else {
				$status = preg_replace('/\s/', '', $status);
			}

			$ret['status'] = $status;
			$ret['format'] = $format;

			// team 1
			$team1 = $dom->find('.match_names .team', 0);
			$ret['team1'] = [
				'name' => trim($team1->find('a', 0)->plaintext),
				'uri' => $team1->find('a', 0)->href,
				'from' => $team1->find('img', 0)->alt
			];

			// team 2
			$team2 = $dom->find('.match_names .team', 1);
			$ret['team2'] = [
				'name' => trim($team2->find('a', 0)->plaintext),
				'uri' => $team2->find('a', 0)->href,
				'from' => ($f = $team2->find('img', 0)) ? $f->alt : ''
			];

			$ret['played'] = $this->getPickoBans($dom);

			$dom->clear();
			unset($dom);

			return $ret;

		} else {
			throw new \Exception('Could not load match HTML '.$uri);
		}
	}

	/** @return array */
	private function getPickoBans(\simple_html_dom $dom)
	{
		$played = [];
		foreach ($dom->find('.submatch') as $gameDiv) {
			$bans1 = [];
			foreach ($gameDiv->find('.ban_left span') as $banSpan) {
				$bans1[] = $banSpan->title;
			}

			$bans2 = [];
			foreach ($gameDiv->find('.ban_right span') as $banSpan) {
				$bans2[] = $banSpan->title;
			}

			$picks1 = [];
			foreach ($gameDiv->find('.pick_left img') as $pickImg) {
				$picks1[] = $pickImg->title;
			}

			$players1 = [];
			foreach ($gameDiv->find('.pick_left a') as $playerA) {
				$players1[] = [
					'uri' => $playerA->href,
					'nickname' => trim($playerA->plaintext)
				];
			}

			$players2 = [];
			foreach ($gameDiv->find('.pick_right a') as $playerA) {
				$players2[] = [
					'uri' => $playerA->href,
					'nickname' => trim($playerA->plaintext)
				];
			}

			$picks2 = [];
			foreach ($gameDiv->find('.pick_right img') as $pickImg) {
				$picks2[] = $pickImg->title;
			}

			$result = trim($gameDiv->find('.score span', 0)->plaintext);

			$played[] = [
				'bans1' => $bans1,
				'bans2' => $bans2,
				'picks1' => $picks1,
				'picks2' => $picks2,
				'players1' => $players1,
				'players2' => $players2,
				'result' => $result,
			];
		}

		return $played;
	}

	public function getTeam($uri)
	{
		$dom = new \simple_html_dom();
		list($html, $uri) = $this->http->loadUri($uri);
		if ($html === false) {
			throw new \Exception('Could not load team HTML '.$uri);
		}

		$dom->load($html);
		$nameDom = $dom->find('div#content h1', 0);

		if (!$nameDom) {
			throw new \Exception('Could not parse team HTML '.$uri);
		}

		$data = [
			'uri' => $uri,
			'fullName' => trim($nameDom->find('text', 0)->plaintext),
			'shortName' => trim($nameDom->find('small', 0)->plaintext, '() '),
//				'logoUri' => $dom->find('.edb_team_head img', 0)->src,
			'from' => $dom->find('.edb_team_head .stats img', 0)->alt
		];
		$roster = [];
		foreach ($dom->find('div#content .pad div .edb_enemies') as $domPlayer) {
			$roster[] = [
				'uri' => $domPlayer->find('a', 0)->href,
				'from' => ($img = $domPlayer->find('.text img', 0)) ? $img->alt : '',
				'nickname' => trim($domPlayer->find('.text a', 0)->plaintext),
				'name' => trim($domPlayer->find('.text strong', 0)->plaintext),
				'role' => trim($domPlayer->find('.text', 0)->find('text', -1)->plaintext)
			];
		}
		$data['roster'] = $roster;

		$dom->clear();
		unset($dom);

		return $data;
	}
}
