<?php

namespace DotaFan\DataProviderBundle\Parser\JoinDota\Exception;

class GameNotFoundException extends \Exception
{
	private $uri;

	public function getUri()
	{
		return $this->uri;
	}

	public function setUri($uri)
	{
		$this->uri = $uri;
	}
}