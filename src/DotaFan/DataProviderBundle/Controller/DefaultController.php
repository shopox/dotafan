<?php

namespace DotaFan\DataProviderBundle\Controller;

use DotaFan\DataProviderBundle\Entity\DotaTeamRepository;
use DotaFan\MainBundle\Entity\Team;
use DotaFan\MainBundle\Entity\TeamRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DotaFanDataProviderBundle:Default:index.html.twig', array('name' => $name));
    }

	public function teamsAction($page, Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		/** @var TeamRepository $teamRepository */
		$teamRepository = $em->getRepository('DotaFanMainBundle:Team');

		/** @var DotaTeamRepository $dotaTeamRepository */
		$dotaTeamRepository = $em->getRepository('DotaFanDataProviderBundle:DotaTeam');

		$empty = (bool) $request->get('empty');

		$teams = $teamRepository->getTeams($page, $empty);

		foreach ($teams as $team) {
			/** @var Team $team */
			$candidates = $dotaTeamRepository->getCandidatesForTeam($team);
			foreach ($candidates as $dotaTeam) {
				$team->addCandidate($dotaTeam);
			}
		}

		return $this->render(
			'DotaFanDataProviderBundle:Default:teams.html.twig',
			['teams' => $teams]
		);
	}

	public function saveTeamsAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();

		/** @var TeamRepository $teamRepository */
		$teamRepository = $em->getRepository('DotaFanMainBundle:Team');

		$data = $request->get('data');
		if (is_array($data)) {
			foreach ($data as $item) {
				$team = $teamRepository->find($item['id']);
				if ($team) {
					/** @var Team $team */
					$team->setDotaTeamId($item['team_id']);

					$em->persist($team);
				}
			}
		}

		$em->flush();

		return new JsonResponse();
	}
}
