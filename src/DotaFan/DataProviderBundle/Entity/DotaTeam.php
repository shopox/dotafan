<?php

namespace DotaFan\DataProviderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * DotaTeam
 */
class DotaTeam
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $teamId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $tag;

    /**
     * @var \DateTime
     */
    private $timeCreated;

    /**
     * @var integer
     */
    private $logo;

    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $json;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set teamId
     *
     * @param integer $teamId
     * @return DotaTeam
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * Get teamId
     *
     * @return integer 
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return DotaTeam
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return DotaTeam
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set timeCreated
     *
     * @param \DateTime $timeCreated
     * @return DotaTeam
     */
    public function setTimeCreated($timeCreated)
    {
        $this->timeCreated = $timeCreated;

        return $this;
    }

    /**
     * Get timeCreated
     *
     * @return \DateTime 
     */
    public function getTimeCreated()
    {
        return $this->timeCreated;
    }

    /**
     * Set logo
     *
     * @param integer $logo
     * @return DotaTeam
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return integer 
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return DotaTeam
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set json
     *
     * @param string $json
     * @return DotaTeam
     */
    public function setJson($json)
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Get json
     *
     * @return string 
     */
    public function getJson()
    {
        return $this->json;
    }
    /**
     * @var string
     */
    private $logoFilename;


    /**
     * Set logoFilename
     *
     * @param string $logoFilename
     * @return DotaTeam
     */
    public function setLogoFilename($logoFilename)
    {
        $this->logoFilename = $logoFilename;

        return $this;
    }

    /**
     * Get logoFilename
     *
     * @return string 
     */
    public function getLogoFilename()
    {
        return $this->logoFilename;
    }
}
