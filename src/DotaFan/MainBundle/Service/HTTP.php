<?php

namespace DotaFan\MainBundle\Service;

class HTTP
{
	/** @return array */
	public function loadUri($uri)
	{
		$options = array(
			CURLOPT_RETURNTRANSFER => true,     // return web page
			CURLOPT_HEADER         => false,    // do not return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
//			CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.114 Safari/537.36", // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 10,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_SSL_VERIFYHOST => 0
		);
		$ch      = curl_init( $uri );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );

		return [$content, $header['url'], $err, $errmsg];
	}

	/**
	 * @return int The function returns the number of bytes that were written to the file, or
	 * false on failure.
	 */
	/*public function downloadFile($uri, $path)
	{
		return file_put_contents($path, fopen($uri, 'r'));
	}*/

	public function get($uri)
	{
		list($content) = $this->loadUri($uri);
		return $content;
	}

	public function downloadFile($uri, $path)
	{
		$fp = fopen($path, 'w+'); //This is the file where we save the    information
		$ch = curl_init($uri); //Here is the file we are downloading, replace spaces with %20
		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_exec($ch); // get curl response
		$err = curl_errno($ch);
		curl_close($ch);
		fclose($fp);

		if ($err == 0) {
			return filesize($path);
		} else {
			return false;
		}
	}
}