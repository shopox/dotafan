<?php

namespace DotaFan\MainBundle\Service;

class SteamApi
{
	private $apiKey;
	private $logoDir;
	private $http;

	public function __construct($apiKey, $logoDir, HTTP $http)
	{
		$this->apiKey = $apiKey;
		$this->logoDir = $logoDir;
		$this->http = $http;
	}

	public function getMatches($startId = null, $leagueId = null, $limit = 100)
	{
		$url = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v001/?'
			.'key='.$this->apiKey
			.'&matches_requested='.$limit
			;

		if (!is_null($startId)) {
			$url .= '&start_at_match_id='.$startId;
		}

		if (!is_null($leagueId)) {
			$url .= '&league_id='.$leagueId;
		}

		$content = $this->http->get($url);

		if ($content !== false) {
			$res = $this->parseResult($content);
			if (isset($res->result) && isset($res->result->matches)) {
				return $res->result->matches;
			}

			return null;
		} else {
			throw new \Exception('Could not load tournament matches JSON');
		}
	}

	public function getMatch($matchId)
	{
		$url = 'http://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v001/?'
			.'key='.$this->apiKey
			.'&match_id='.$matchId
		;

		$content = $this->http->get($url);
		if ($content !== false) {
			$res = $this->parseResult($content);
			return $res->result;
		} else {
			throw new \Exception('Could not load match JSON');
		}
	}

	public function getTeam($teamId = null)
	{
		$result = $this->getTeams($teamId, 1);
		if ($result->status == 1) {
			$teams = $result->teams;
			if (isset($teams[0]) && $teams[0]->team_id == $teamId) {
				return $teams[0];
			}
		}
		return null;
	}

	public function getTeams($teamId = null, $limit = 1)
	{
		$url = 'https://api.steampowered.com/IDOTA2Match_570/GetTeamInfoByTeamID/v001/?'
			.'key='.$this->apiKey
			.'&teams_requested='.$limit
		;
		if (!is_null($teamId)) {
			$url .= '&start_at_team_id='.$teamId;
		}

		$content = $this->http->get($url);
		if ($content !== false) {
			$res = $this->parseResult($content);
			return $res->result;
		} else {
			throw new \Exception('Could not load match JSON');
		}
	}

	private function parseResult($content)
	{
		$response = json_decode($content, false, 512, JSON_BIGINT_AS_STRING);
		if ($response instanceof \stdClass) {
			return $response;
		} else {
			throw new \Exception('Could not parse JSON');
		}
	}

	public function downloadLogo($logo)
	{
		$url = 'http://api.steampowered.com/ISteamRemoteStorage/GetUGCFileDetails/v1/'
			.'?key='.$this->apiKey
			.'&appid=570'
			.'&ugcid='.$logo;

		$content = $this->http->get($url);
		if ($content !== false) {
			$res = $this->parseResult($content);
			if (isset($res->data) && isset($res->data->url)) {
				$path = tempnam($this->logoDir, 'downloaded_logo_');
				$bytes = $this->http->downloadFile($res->data->url, $path);
				if ($bytes !== false && $bytes == $res->data->size) {
					return $path;
				} else {
//					throw new \Exception('Could not download logo image');
					return false;
				}
			} else {
				return false; // team had no logo
			}
		} else {
			throw new \Exception('Could not load logo JSON');
		}
	}

	public function getTeamLogoFilePath($teamId)
	{
		return $this->logoDir .DIRECTORY_SEPARATOR. $this->getTeamLogoFileName($teamId);
	}

	public function getTeamLogoFileName($teamId)
	{
		return 'team_id_'.$teamId.'.png';
	}

	/** @return array */
	public function getLeagues()
	{
		$url = 'https://api.steampowered.com/IDOTA2Match_570/GetLeagueListing/v001/?'
			.'key='.$this->apiKey
		;

		$content = $this->http->get($url);
		if ($content !== false) {
			$res = $this->parseResult($content);

			if (isset($res->result) && isset($res->result->leagues)) {
				return $res->result->leagues;
			}

			return null;

		} else {
			throw new \Exception('Could not load leagues JSON');
		}
	}
}