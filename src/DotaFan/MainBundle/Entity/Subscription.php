<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Subscription
 */
class Subscription
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $tournamentId;

    /**
     * @var integer
     */
    private $teamId;

    /**
     * @var integer
     */
    private $gameId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Subscription
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set tournamentId
     *
     * @param integer $tournamentId
     * @return Subscription
     */
    public function setTournamentId($tournamentId)
    {
        $this->tournamentId = $tournamentId;

        return $this;
    }

    /**
     * Get tournamentId
     *
     * @return integer 
     */
    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    /**
     * Set teamId
     *
     * @param integer $teamId
     * @return Subscription
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * Get teamId
     *
     * @return integer 
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     * @return Subscription
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer 
     */
    public function getGameId()
    {
        return $this->gameId;
    }
    /**
     * @var \DotaFan\MainBundle\Entity\Tournament
     */
    private $tournament;

    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $team;

    /**
     * @var \DotaFan\MainBundle\Entity\Game
     */
    private $game;

    /**
     * @var \DotaFan\MainBundle\Entity\User
     */
    private $user;


    /**
     * Set tournament
     *
     * @param \DotaFan\MainBundle\Entity\Tournament $tournament
     * @return Subscription
     */
    public function setTournament(\DotaFan\MainBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \DotaFan\MainBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }

    /**
     * Set team
     *
     * @param \DotaFan\MainBundle\Entity\Team $team
     * @return Subscription
     */
    public function setTeam(\DotaFan\MainBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set game
     *
     * @param \DotaFan\MainBundle\Entity\Game $game
     * @return Subscription
     */
    public function setGame(\DotaFan\MainBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \DotaFan\MainBundle\Entity\Game 
     */
    public function getGame()
    {
        return $this->game;
    }

    /**
     * Set user
     *
     * @param \DotaFan\MainBundle\Entity\User $user
     * @return Subscription
     */
    public function setUser(\DotaFan\MainBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \DotaFan\MainBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
