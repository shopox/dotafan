<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Team
 */
class Team
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $uri;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $shortName;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Team
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Team
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set shortName
     *
     * @param string $shortName
     * @return Team
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * Get shortName
     *
     * @return string 
     */
    public function getShortName()
    {
        return $this->shortName;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $players;

	/**
	 * @var \Doctrine\Common\Collections\Collection
	 */
	private $candidates;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->players = new \Doctrine\Common\Collections\ArrayCollection();
        $this->candidates = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add players
     *
     * @param \DotaFan\MainBundle\Entity\Player $players
     * @return Team
     */
    public function addPlayer(\DotaFan\MainBundle\Entity\Player $players)
    {
        $this->players[] = $players;

        return $this;
    }

    /**
     * Remove players
     *
     * @param \DotaFan\MainBundle\Entity\Player $players
     */
    public function removePlayer(\DotaFan\MainBundle\Entity\Player $players)
    {
        $this->players->removeElement($players);
    }

    /**
     * Get players
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPlayers()
    {
        return $this->players;
    }
    /**
     * @var \DateTime
     */
    private $last_match_date;


    /**
     * Set last_match_date
     *
     * @param \DateTime $lastMatchDate
     * @return Team
     */
    public function setLastMatchDate($lastMatchDate)
    {
        $this->last_match_date = $lastMatchDate;

        return $this;
    }

    /**
     * Get last_match_date
     *
     * @return \DateTime 
     */
    public function getLastMatchDate()
    {
        return $this->last_match_date;
    }
    /**
     * @var integer
     */
    private $rating;


    /**
     * Set rating
     *
     * @param integer $rating
     * @return Team
     */
    public function setRating($rating)
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * Get rating
     *
     * @return integer 
     */
    public function getRating()
    {
        return $this->rating;
    }

	/**
	 * Get candidates
	 *
	 * @return \Doctrine\Common\Collections\Collection
	 */
	public function getCandidates()
	{
		return $this->candidates;
	}

	/**
	 * Add candidate
	 *
	 * @param \DotaFan\DataProviderBundle\Entity\DotaTeam $candidate
	 * @return Team
	 */
	public function addCandidate(\DotaFan\DataProviderBundle\Entity\DotaTeam $candidate)
	{
		$this->candidates[] = $candidate;

		return $this;
	}
    /**
     * @var integer
     */
    private $dotaTeamId;

    /**
     * @var \DotaFan\DataProviderBundle\Entity\DotaTeam
     */
    private $dotaTeam;


    /**
     * Set dotaTeamId
     *
     * @param integer $dotaTeamId
     * @return Team
     */
    public function setDotaTeamId($dotaTeamId)
    {
        $this->dotaTeamId = $dotaTeamId;

        return $this;
    }

    /**
     * Get dotaTeamId
     *
     * @return integer 
     */
    public function getDotaTeamId()
    {
        return $this->dotaTeamId;
    }

    /**
     * Set dotaTeam
     *
     * @param \DotaFan\DataProviderBundle\Entity\DotaTeam $dotaTeam
     * @return Team
     */
    public function setDotaTeam(\DotaFan\DataProviderBundle\Entity\DotaTeam $dotaTeam = null)
    {
        $this->dotaTeam = $dotaTeam;

        return $this;
    }

    /**
     * Get dotaTeam
     *
     * @return \DotaFan\DataProviderBundle\Entity\DotaTeam 
     */
    public function getDotaTeam()
    {
        return $this->dotaTeam;
    }

	public function getLogoUrl()
	{
		if ($this->dotaTeam) {
			return sprintf('/teamlogo/team_id_%d.png', $this->dotaTeam->getTeamId());
		}

		return null;
	}
    /**
     * @var \DateTime
     */
    private $updateDate;


    /**
     * Set updateDate
     *
     * @param \DateTime $updateDate
     * @return Team
     */
    public function setUpdateDate($updateDate)
    {
        $this->updateDate = $updateDate;

        return $this;
    }

    /**
     * Get updateDate
     *
     * @return \DateTime 
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }
}
