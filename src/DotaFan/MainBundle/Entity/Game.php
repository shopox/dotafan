<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Game
 */
class Game
{

	const FINISHED_NOT_PLAYED = 0;
	const FINISHED_RESULT = 1;
	const FINISHED_CANCELED = 2;
	const FINISHED_POSTPONED = 3;
	const FINISHED_TBA = 4;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var string
     */
    private $result;

    /**
     * @var string
     */
    private $format;

    /**
     * @var integer
     */
    private $tournamentId;

    /**
     * @var integer
     */
    private $team1Id;

    /**
     * @var integer
     */
    private $team2Id;

    /**
     * @var string
     */
    private $uri;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Game
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return Game
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return Game
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set tournamentId
     *
     * @param integer $tournamentId
     * @return Game
     */
    public function setTournamentId($tournamentId)
    {
        $this->tournamentId = $tournamentId;

        return $this;
    }

    /**
     * Get tournamentId
     *
     * @return integer 
     */
    public function getTournamentId()
    {
        return $this->tournamentId;
    }

    /**
     * Set team1Id
     *
     * @param integer $team1Id
     * @return Game
     */
    public function setTeam1Id($team1Id)
    {
        $this->team1Id = $team1Id;

        return $this;
    }

    /**
     * Get team1Id
     *
     * @return integer 
     */
    public function getTeam1Id()
    {
        return $this->team1Id;
    }

    /**
     * Set team2Id
     *
     * @param integer $team2Id
     * @return Game
     */
    public function setTeam2Id($team2Id)
    {
        $this->team2Id = $team2Id;

        return $this;
    }

    /**
     * Get team2Id
     *
     * @return integer 
     */
    public function getTeam2Id()
    {
        return $this->team2Id;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Game
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }
    /**
     * @var \DotaFan\MainBundle\Entity\Tournament
     */
    private $tournament;


    /**
     * Set tournament
     *
     * @param \DotaFan\MainBundle\Entity\Tournament $tournament
     * @return Game
     */
    public function setTournament(\DotaFan\MainBundle\Entity\Tournament $tournament = null)
    {
        $this->tournament = $tournament;

        return $this;
    }

    /**
     * Get tournament
     *
     * @return \DotaFan\MainBundle\Entity\Tournament 
     */
    public function getTournament()
    {
        return $this->tournament;
    }
    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $team1;

    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $team2;


    /**
     * Set team1
     *
     * @param \DotaFan\MainBundle\Entity\Team $team1
     * @return Game
     */
    public function setTeam1(\DotaFan\MainBundle\Entity\Team $team1 = null)
    {
        $this->team1 = $team1;

        return $this;
    }

    /**
     * Get team1
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getTeam1()
    {
        return $this->team1;
    }

    /**
     * Set team2
     *
     * @param \DotaFan\MainBundle\Entity\Team $team2
     * @return Game
     */
    public function setTeam2(\DotaFan\MainBundle\Entity\Team $team2 = null)
    {
        $this->team2 = $team2;

        return $this;
    }

    /**
     * Get team2
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getTeam2()
    {
        return $this->team2;
    }
    /**
     * @var string
     */
    private $bracket;


    /**
     * Set bracket
     *
     * @param string $bracket
     * @return Game
     */
    public function setBracket($bracket)
    {
        $this->bracket = $bracket;

        return $this;
    }

    /**
     * Get bracket
     *
     * @return string 
     */
    public function getBracket()
    {
        return $this->bracket;
    }
    /**
     * @var boolean
     */
    private $finished;


    /**
     * Set finished
     *
     * @param boolean $finished
     * @return Game
     */
    public function setFinished($finished)
    {
		$allowed = [
			self::FINISHED_NOT_PLAYED,
			self::FINISHED_RESULT,
			self::FINISHED_CANCELED,
			self::FINISHED_POSTPONED,
			self::FINISHED_TBA
		];

		if (!in_array($finished, $allowed)) {
			throw new \InvalidArgumentException('Invalid finished flag');
		}

        $this->finished = $finished;

        return $this;
    }

    /**
     * Get finished
     *
     * @return boolean 
     */
    public function getFinished()
    {
        return $this->finished;
    }

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $maps;


    /**
     * Add maps
     *
     * @param \DotaFan\MainBundle\Entity\GameMap $maps
     * @return Game
     */
    public function addMap(\DotaFan\MainBundle\Entity\GameMap $maps)
    {
        $this->maps[] = $maps;

        return $this;
    }

    /**
     * Remove maps
     *
     * @param \DotaFan\MainBundle\Entity\GameMap $maps
     */
    public function removeMap(\DotaFan\MainBundle\Entity\GameMap $maps)
    {
        $this->maps->removeElement($maps);
    }

    /**
     * Get maps
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMaps()
    {
        return $this->maps;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->maps = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @var integer
     */
    private $winnerId;

    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $winner;


    /**
     * Set winnerId
     *
     * @param integer $winnerId
     * @return Game
     */
    public function setWinnerId($winnerId)
    {
        $this->winnerId = $winnerId;

        return $this;
    }

    /**
     * Get winnerId
     *
     * @return integer 
     */
    public function getWinnerId()
    {
        return $this->winnerId;
    }

    /**
     * Set winner
     *
     * @param \DotaFan\MainBundle\Entity\Team $winner
     * @return Game
     */
    public function setWinner(\DotaFan\MainBundle\Entity\Team $winner = null)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getWinner()
    {
        return $this->winner;
    }
}
