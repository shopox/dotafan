<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameBan
 */
class GameBan
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $n;

    /**
     * @var string
     */
    private $hero;

    /**
     * @var integer
     */
    private $teamId;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     * @return GameBan
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer 
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set n
     *
     * @param integer $n
     * @return GameBan
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get n
     *
     * @return integer 
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set hero
     *
     * @param string $hero
     * @return GameBan
     */
    public function setHero($hero)
    {
        $this->hero = $hero;

        return $this;
    }

    /**
     * Get hero
     *
     * @return string 
     */
    public function getHero()
    {
        return $this->hero;
    }

    /**
     * Set teamId
     *
     * @param integer $teamId
     * @return GameBan
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * Get teamId
     *
     * @return integer 
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $team;

    /**
     * Set team
     *
     * @param \DotaFan\MainBundle\Entity\Team $team
     * @return GameBan
     */
    public function setTeam(\DotaFan\MainBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }
    /**
     * @var integer
     */
    private $mapId;

    /**
     * @var \DotaFan\MainBundle\Entity\GameMap
     */
    private $map;


    /**
     * Set mapId
     *
     * @param integer $mapId
     * @return GameBan
     */
    public function setMapId($mapId)
    {
        $this->mapId = $mapId;

        return $this;
    }

    /**
     * Get mapId
     *
     * @return integer 
     */
    public function getMapId()
    {
        return $this->mapId;
    }

    /**
     * Set map
     *
     * @param \DotaFan\MainBundle\Entity\GameMap $map
     * @return GameBan
     */
    public function setMap(\DotaFan\MainBundle\Entity\GameMap $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \DotaFan\MainBundle\Entity\GameMap 
     */
    public function getMap()
    {
        return $this->map;
    }
}
