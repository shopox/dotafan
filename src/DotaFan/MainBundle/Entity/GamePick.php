<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GamePick
 */
class GamePick
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $mapId;

    /**
     * @var integer
     */
    private $teamId;

    /**
     * @var integer
     */
    private $playerId;

    /**
     * @var integer
     */
    private $n;

    /**
     * @var string
     */
    private $hero;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mapId
     *
     * @param integer $mapId
     * @return GamePick
     */
    public function setMapId($mapId)
    {
        $this->mapId = $mapId;

        return $this;
    }

    /**
     * Get mapId
     *
     * @return integer 
     */
    public function getMapId()
    {
        return $this->mapId;
    }

    /**
     * Set teamId
     *
     * @param integer $teamId
     * @return GamePick
     */
    public function setTeamId($teamId)
    {
        $this->teamId = $teamId;

        return $this;
    }

    /**
     * Get teamId
     *
     * @return integer 
     */
    public function getTeamId()
    {
        return $this->teamId;
    }

    /**
     * Set playerId
     *
     * @param integer $playerId
     * @return GamePick
     */
    public function setPlayerId($playerId)
    {
        $this->playerId = $playerId;

        return $this;
    }

    /**
     * Get playerId
     *
     * @return integer 
     */
    public function getPlayerId()
    {
        return $this->playerId;
    }

    /**
     * Set n
     *
     * @param integer $n
     * @return GamePick
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get n
     *
     * @return integer 
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set hero
     *
     * @param string $hero
     * @return GamePick
     */
    public function setHero($hero)
    {
        $this->hero = $hero;

        return $this;
    }

    /**
     * Get hero
     *
     * @return string 
     */
    public function getHero()
    {
        return $this->hero;
    }
    /**
     * @var \DotaFan\MainBundle\Entity\Team
     */
    private $team;

    /**
     * @var \DotaFan\MainBundle\Entity\GameMap
     */
    private $map;

    /**
     * @var \DotaFan\MainBundle\Entity\Player
     */
    private $player;


    /**
     * Set team
     *
     * @param \DotaFan\MainBundle\Entity\Team $team
     * @return GamePick
     */
    public function setTeam(\DotaFan\MainBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \DotaFan\MainBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set map
     *
     * @param \DotaFan\MainBundle\Entity\GameMap $map
     * @return GamePick
     */
    public function setMap(\DotaFan\MainBundle\Entity\GameMap $map = null)
    {
        $this->map = $map;

        return $this;
    }

    /**
     * Get map
     *
     * @return \DotaFan\MainBundle\Entity\GameMap 
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * Set player
     *
     * @param \DotaFan\MainBundle\Entity\Player $player
     * @return GamePick
     */
    public function setPlayer(\DotaFan\MainBundle\Entity\Player $player = null)
    {
        $this->player = $player;

        return $this;
    }

    /**
     * Get player
     *
     * @return \DotaFan\MainBundle\Entity\Player 
     */
    public function getPlayer()
    {
        return $this->player;
    }
}
