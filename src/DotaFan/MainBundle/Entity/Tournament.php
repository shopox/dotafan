<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tournament
 */
class Tournament
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $uri;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Tournament
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set uri
     *
     * @param string $uri
     * @return Tournament
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri
     *
     * @return string 
     */
    public function getUri()
    {
        return $this->uri;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $games;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add games
     *
     * @param \DotaFan\MainBundle\Entity\Game $games
     * @return Tournament
     */
    public function addGame(\DotaFan\MainBundle\Entity\Game $games)
    {
        $this->games[] = $games;

        return $this;
    }

    /**
     * Remove games
     *
     * @param \DotaFan\MainBundle\Entity\Game $games
     */
    public function removeGame(\DotaFan\MainBundle\Entity\Game $games)
    {
        $this->games->removeElement($games);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGames()
    {
        return $this->games;
    }
    /**
     * @var \DateTime
     */
    private $last_match_date;


    /**
     * Set last_match_date
     *
     * @param \DateTime $lastMatchDate
     * @return Tournament
     */
    public function setLastMatchDate($lastMatchDate)
    {
        $this->last_match_date = $lastMatchDate;

        return $this;
    }

    /**
     * Get last_match_date
     *
     * @return \DateTime 
     */
    public function getLastMatchDate()
    {
        return $this->last_match_date;
    }
}
