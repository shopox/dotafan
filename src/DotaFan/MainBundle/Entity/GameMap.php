<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * GameMap
 */
class GameMap
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $gameId;

    /**
     * @var integer
     */
    private $n;

    /**
     * @var string
     */
    private $result;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     * @return GameMap
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer 
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set n
     *
     * @param integer $n
     * @return GameMap
     */
    public function setN($n)
    {
        $this->n = $n;

        return $this;
    }

    /**
     * Get n
     *
     * @return integer 
     */
    public function getN()
    {
        return $this->n;
    }

    /**
     * Set result
     *
     * @param string $result
     * @return GameMap
     */
    public function setResult($result)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result
     *
     * @return string 
     */
    public function getResult()
    {
        return $this->result;
    }
    /**
     * @var \DotaFan\MainBundle\Entity\Game
     */
    private $game;


    /**
     * Set game
     *
     * @param \DotaFan\MainBundle\Entity\Game $game
     * @return GameMap
     */
    public function setGame(\DotaFan\MainBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \DotaFan\MainBundle\Entity\Game 
     */
    public function getGame()
    {
        return $this->game;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bans;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->bans = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add bans
     *
     * @param \DotaFan\MainBundle\Entity\GameBan $bans
     * @return GameMap
     */
    public function addBan(\DotaFan\MainBundle\Entity\GameBan $bans)
    {
        $this->bans[] = $bans;

        return $this;
    }

    /**
     * Remove bans
     *
     * @param \DotaFan\MainBundle\Entity\GameBan $bans
     */
    public function removeBan(\DotaFan\MainBundle\Entity\GameBan $bans)
    {
        $this->bans->removeElement($bans);
    }

    /**
     * Get bans
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBans()
    {
        return $this->bans;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $picks;


    /**
     * Add picks
     *
     * @param \DotaFan\MainBundle\Entity\GamePick $picks
     * @return GameMap
     */
    public function addPick(\DotaFan\MainBundle\Entity\GamePick $picks)
    {
        $this->picks[] = $picks;

        return $this;
    }

    /**
     * Remove picks
     *
     * @param \DotaFan\MainBundle\Entity\GamePick $picks
     */
    public function removePick(\DotaFan\MainBundle\Entity\GamePick $picks)
    {
        $this->picks->removeElement($picks);
    }

    /**
     * Get picks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPicks()
    {
        return $this->picks;
    }
}
