<?php

namespace DotaFan\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TournamentRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TournamentRepository extends EntityRepository
{
	/** @return Tournament */
	public function findByUriOrCreate($uri)
	{
		$tournament = $this->findOneByUri($uri);
		if (!$tournament) {
			$tournament = new Tournament();
			$tournament->setUri($uri);
		}
		return $tournament;
	}
}
