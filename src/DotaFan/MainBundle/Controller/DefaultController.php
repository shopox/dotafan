<?php

namespace DotaFan\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('DotaFanMainBundle:Default:index.html.twig');
    }

	public function flagsAction()
	{
		$sql = "
			SELECT DISTINCT country FROM team
			UNION
			SELECT DISTINCT country FROM player
			ORDER BY 1
			";

		$stmt = $this->getDoctrine()->getManager()->getConnection()->prepare($sql);
		$stmt->execute();

		$countries = $stmt->fetchAll();

		return $this->render('DotaFanMainBundle:Default:flags.html.twig', compact('countries'));
	}
}
