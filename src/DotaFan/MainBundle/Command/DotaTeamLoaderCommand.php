<?php
namespace DotaFan\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use DotaFan\MainBundle\Service\SteamApi;
use DotaFan\MainBundle\Service\HTTP;
use DotaFan\DataProviderBundle\Entity\DotaTeam;
use DotaFan\DataProviderBundle\Entity\DotaTeamRepository;
use DotaFan\DataProviderBundle\Entity\DotaLeague;
use DotaFan\DataProviderBundle\Entity\DotaLeagueRepository;
use Doctrine\ORM\EntityManager;

class DotaTeamLoaderCommand extends ContainerAwareCommand
{
	private $doctrine;

	/** @var EntityManager $em */
	private $em;

	/** @var DotaTeamRepository $dotaTeamRepository */
	private $dotaTeamRepository;

	/** @var DotaLeagueRepository $dotaLeagueRepository */
	private $dotaLeagueRepository;

	/** @var SteamApi $api */
	private $api;

	/** @var HTTP $http */
	private $http;

	private $input;
	private $output;

	protected function configure()
	{
		$this
			->setName('main:teamloader')
			->setDescription('Steam WEB API Dota 2 team loader')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->doctrine = $this->getContainer()->get('doctrine');
		$this->em = $this->doctrine->getManager();
		$this->dotaTeamRepository = $this->doctrine->getRepository('DotaFanDataProviderBundle:DotaTeam');
		$this->dotaLeagueRepository = $this->doctrine->getRepository('DotaFanDataProviderBundle:DotaLeague');
		$this->api = $this->getContainer()->get('dota_fan_main.steamapi');
		$this->http = $this->getContainer()->get('dota_fan_main.http');

		$this->input = $input;
		$this->output = $output;

		$this->loadTeams();
//		$this->loadLogos();

//		$this->loadLeagues();
//		$this->loadTeamsByLeagueMatches();
	}

	private function loadTeams()
	{
		$tidfile = __DIR__.'/team.id';

		$startId = file_get_contents($tidfile);;
		if (!$startId)
			$startId = null;

		$parsed = 0;
		while (true) {
			try {

				$teamsResult = $this->api->getTeams($startId, 100);
				if ($teamsResult->status == 1) {

					for ($n = 0; $n < count($teamsResult->teams);) {

						$teamJson = $teamsResult->teams[$n];
						$tid = $teamJson->team_id;
						$this->output->writeln("team_id=".$tid);

						try {

							if (isset($teamJson->league_id_0)) {

								$team = $this->dotaTeamRepository->findByTeamId($tid);
								if (!$team) {
									$this->em->getConnection()->beginTransaction();

									$team = $this->createTeam($teamJson);
									$this->em->persist($team);

									$this->em->flush();
									$this->em->getConnection()->commit();

									$this->output->writeln("created!");
								} else {
									$this->output->writeln('already loaded');
								}

							}

							$startId = $tid;


							$n++;

							$parsed++;
							$this->output->writeln($parsed.' teams parsed');
							$this->output->writeln('');

						} catch (\Exception $e) {
							$this->output->writeln($e->getMessage());

							$this->em->clear();
							$this->em->getConnection()->close();
							$this->em->getConnection()->connect();

							if (!$this->em->isOpen()) {
								$this->em = $this->em->create(
									$this->em->getConnection(),
									$this->em->getConfiguration()
								);
							}
						}

					}

					file_put_contents($tidfile, $startId);

					if (count($teamsResult->teams) == 0) {
						break;
					}
				} else {
					continue;
				}

			} catch (\Exception $e) {
				$this->output->writeln($e->getMessage());
			}
		}
	}

	private function loadTeamsByTournamentGames()
	{

		$midfile = __DIR__.'/match.id';

		$startId = file_get_contents($midfile);;
		if (!$startId)
			$startId = null;

		$parsed = 0;
		while (true) {
			try {

				$matchesResult = $this->api->getMatches($startId);
				if ($matchesResult->status == 1) {

					for ($n = 0; $n < count($matchesResult->matches);) {

						$match = $matchesResult->matches[$n];
						$output->writeln("match_id=".$match->match_id);

						try {

							$this->em->getConnection()->beginTransaction();

							$matchResult = $this->api->getMatch($match->match_id);

							// Radiant team
							if (isset($matchResult->radiant_team_id)) {
								$tid = $matchResult->radiant_team_id;
								$output->writeln("radiant_team_id=".$tid);

								$rteam = $this->dotaTeamRepository->findByTeamId($tid);
								if (!$rteam) {
									$teamJson = $this->api->getTeam($tid);
									if (!is_null($teamJson)) {
										$rteam = $this->createTeam($teamJson);
										$this->em->persist($rteam);
										$output->writeln("created!");
									} else {
										$output->writeln("not found!");
									}
								} else {
									$output->writeln('already loaded');
								}
							}

							// Dire team
							if (isset($matchResult->dire_team_id)) {
								$tid = $matchResult->dire_team_id;
								$output->writeln("dire_team_id=".$tid);

								$dteam = $this->dotaTeamRepository->findByTeamId($tid);
								if (!$dteam) {
									$teamJson = $this->api->getTeam($tid);
									if (!is_null($teamJson)) {
										$dteam = $this->createTeam($teamJson);
										$this->em->persist($dteam);
										$output->writeln("created!");
									} else {
										$output->writeln("not found!");
									}
								} else {
									$output->writeln('already loaded');
								}
							}

							$this->em->flush();
							$this->em->getConnection()->commit();

							$n++;

							$parsed++;
							$output->writeln($parsed.' matches parsed');
							$output->writeln('');

						} catch (\Exception $e) {
							$output->writeln($e->getMessage());

							$this->em->clear();
							$this->em->getConnection()->close();
							$this->em->getConnection()->connect();

							if (!$this->em->isOpen()) {
								$this->em = $this->em->create(
									$this->em->getConnection(),
									$this->em->getConfiguration()
								);
							}
						}

					}

					$startId = $match->match_id;

					file_put_contents($midfile, $startId);

					if ($matchesResult->results_remaining == 0) {
						break;
					}
				} else {
					continue;
				}

			} catch (\Exception $e) {
				$output->writeln($e->getMessage());
			}
		}
	}

	private function updateTeam($teamId)
	{
		$team = $this->dotaTeamRepository->findOneByTeamId($teamId);
		if ($team) {
			$logo = $team->getLogo();
			if ($logo && strpos($logo, '.') === false) {
				return $team;
			}
		} else {
			$team = new DotaTeam();
		}

		$json = $this->api->getTeam($teamId);

		if (!is_null($json)) {

			$this->setTeam($team, $json);

			$logo = $json->logo;
			$team->setLogo($logo);
			$team->setLogoFilename(null);

			if ($logo) {

				$downloadedPath = $this->api->downloadLogo($logo);
				if ($downloadedPath !== false) {
					$savePath = $this->api->getTeamLogoFilePath($team->getTeamId());

					if (!rename($downloadedPath, $savePath)) {
						throw new \Exception('Could not store logo');
					}

					$team->setLogoFilename($this->api->getTeamLogoFileName($team->getTeamId()));

					$this->output->writeln('stored logo for team_id='.$team->getTeamId());
				} else {
					$this->output->writeln('team has no logo team_id='.$team->getTeamId());
				}
			}

			return $team;
		}

		return null;
	}

	private function loadLogos()
	{
		$teams = $this->dotaTeamRepository->findTeamsForParse();
		$i = 0;
		$c = count($teams);
		while ($i < $c) {

			/** @var DotaTeam $team */
			$team = $teams[$i];

			$this->output->writeln('updating team_id='.$team->getTeamId());

			try {

				if (!$team->getLogoFilename()) {

					$json = $this->api->getTeam($team->getTeamId());

					if (!is_null($json)) {

						$logo = $json->logo;
						$team->setLogo($logo);
						if ($logo) {

							$downloadedPath = $this->api->downloadLogo($logo);
							if ($downloadedPath !== false) {
								$savePath = $this->api->getTeamLogoFilePath($team->getTeamId());

								if (!rename($downloadedPath, $savePath)) {
									throw new \Exception('Could not store logo');
								}

								$team->setLogoFilename($this->api->getTeamLogoFileName($team->getTeamId()));

								$this->output->writeln('stored logo for team_id='.$team->getTeamId());
							} else {
								$this->output->writeln('team has no logo team_id='.$team->getTeamId());
								$team->setLogoFilename(null);
							}
						}

						$this->em->persist($team);
						$this->em->flush();
					}
				}

				$i++;

			} catch (\Exception $e) {
				$this->output->writeln($e->getMessage());

				$this->em->getConnection()->close();
				$this->em->getConnection()->connect();
			}
		}
	}

	private function loadLeagues()
	{
		$leagues = $this->api->getLeagues();
		if (!is_null($leagues)) {
			foreach ($leagues as $leagueJson) {

				$league = $this->dotaLeagueRepository->findOneBy(['leagueId' => $leagueJson->leagueid]);
				if (!$league) {
					$league = new DotaLeague();
				}
				$league = $this->setLeague($league, $leagueJson);

				$this->em->persist($league);
				$this->em->flush();

				$this->output->writeln("league_id=".$league->getLeagueId());

			}
		} else {
			throw new \Exception('Could not parse leagues');
		}
	}

	private function loadTeamsByLeagueMatches()
	{
		$leagues = $this->dotaLeagueRepository->getNotableLeagues();
		$c = count($leagues);
		$i = 0;
		while ($i < $c) {

			/** @var DotaLeague $league */
			$league = $leagues[$i];

			try {

				$matches = $this->api->getMatches(null, $league->getLeagueId());

			} catch (\Exception $e) {
				$this->output->writeln($e->getMessage());
				continue;
			}

			if (!is_null($matches)) {

				$mc = count($matches);
				$mi = 0;

				while ($mi < $mc) {

					$matchJson = $matches[$mi];

					try {

						$matchDetailsJson = $this->api->getMatch($matchJson->match_id);

						$radiantTeam = $this->updateTeam($matchDetailsJson->radiant_team_id);
						$direTeam = $this->updateTeam($matchDetailsJson->dire_team_id);

						$this->em->getConnection()->beginTransaction();

						if (!is_null($radiantTeam)) {
							$this->em->persist($radiantTeam);
						}

						if (!is_null($direTeam)) {
							$this->em->persist($direTeam);
						}

						$this->em->flush();
						$this->em->getConnection()->commit();

					} catch (\Exception $e) {
						$this->output->writeln($e->getMessage());
						continue;
					}

					$mi++;
				}

			}

			$i++;

		}
	}

	private function setLeague(DotaLeague $league, $json)
	{
		$name = $json->name;
		$name = str_replace('#DOTA_Item_Dota_2_', '', $name);
		$name = str_replace('#DOTA_Item_League_', '', $name);
		$name = str_replace('#DOTA_Item_', '', $name);
		$name = str_replace('_', ' ', $name);
		$league->setName($name);

		$description = $json->description;
		$description = str_replace('#DOTA_Item_Desc_Dota_2_', '', $description);
		$description = str_replace('#DOTA_Item_Desc_League_', '', $description);
		$description = str_replace('#DOTA_Item_League_', '', $description);
		$description = str_replace('#DOTA_Item_Desc_', '', $description);
		$description = str_replace('#DOTA_Item_', '', $description);
		$description = str_replace('_', ' ', $description);
		$league->setDescription($description);

		$league->setLeagueId($json->leagueid);
		$league->setUrl($json->tournament_url);
		$league->setJson(json_encode($json));

		return $league;
	}

	private function setTeam(DotaTeam $team, $json)
	{
		$team->setTeamId($json->team_id);
		$team->setName($json->name);
		$team->setLogo($json->logo);
		$team->setTag($json->tag);
		$dt = new \DateTime();
		$dt->setTimestamp($json->time_created);
		$team->setTimeCreated($dt);
		$team->setUrl($json->url);
		$team->setJson(json_encode($json));

		return $team;
	}

	private function createTeam($json)
	{
		return $this->setTeam(new DotaTeam(), $json);
	}
}