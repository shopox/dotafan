<?php
namespace DotaFan\MainBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DotaFan\MainBundle\Entity\Game;
use DotaFan\MainBundle\Entity\GameRepository;
use Doctrine\ORM\EntityManager;

class MatchWinnerCommand extends ContainerAwareCommand
{
	protected function configure()
	{
		$this
			->setName('main:matchwinner')
			->setDescription('Set winner of the match')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{

		$doctrine = $this->getContainer()->get('doctrine');

		/** @var EntityManager $em */
		$em = $doctrine->getManager();

		/** @var GameRepository $gameRepository */
		$gameRepository = $doctrine->getRepository('DotaFanMainBundle:Game');

		$c = 0;

		foreach ($gameRepository->findAll() as $game) {

			/** @var Game $game */

			$output->writeln("game_id=".$game->getId());

			if (preg_match('/(\d).*(\d)/', $game->getResult(), $m)) {

				if ($m[1] > $m[2]) {
					$game->setWinner($game->getTeam1());
				} else if ($m[1] < $m[2]) {
					$game->setWinner($game->getTeam2());
				}

				$em->persist($game);
			}

			if (++$c > 1000) {
				$em->flush();
				$c = 0;
			}
		}

		$em->flush();
	}
}