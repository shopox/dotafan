<?php

namespace DotaFan\MainBundle\Command;

use DotaFan\DataProviderBundle\Parser\JoinDota\Exception\GameNotFoundException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DotaFan\DataProviderBundle\Parser\JoinDota\Parser as JoinDotaParser;
use DotaFan\MainBundle\Entity;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraints\DateTime;

class JoinDotaLoaderCommand extends ContainerAwareCommand
{
	private $doctrine;

	/** @var JoinDotaParser */
	private $parser;

	/** @var EntityManager $em */
	private $em;

	/** @var Entity\TournamentRepository $tournamentRepository */
	private $tournamentRepository;

	/** @var Entity\GameRepository $gameRepository */
	private $gameRepository;

	/** @var Entity\TeamRepository $teamRepository */
	private $teamRepository;

	/** @var Entity\PlayerRepository $playerRepository */
	private $playerRepository;

	/** @var Entity\GameMapRepository $mapRepository */
	private $mapRepository;

	/** @var Entity\GameBanRepository $banRepository */
	private $banRepository;

	/** @var Entity\GamePickRepository $pickRepository */
	private $pickRepository;

	private $whole = false;

	protected function configure()
	{
		$this
			->setName('main:jdloader')
			->setDescription('joinDOTA matches loader')
			->addArgument('page', InputArgument::OPTIONAL, 'Page number to start with')
			->addArgument('whole', InputArgument::OPTIONAL, 'Parse whole DB')
		;
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->doctrine = $this->getContainer()->get('doctrine');

		/** @var EntityManager $em */
		$this->em = $this->doctrine->getManager();

		$this->parser = new JoinDotaParser();
		$this->tournamentRepository = $this->doctrine->getRepository('DotaFanMainBundle:Tournament');
		$this->gameRepository = $this->doctrine->getRepository('DotaFanMainBundle:Game');
		$this->teamRepository = $this->doctrine->getRepository('DotaFanMainBundle:Team');
		$this->playerRepository = $this->doctrine->getRepository('DotaFanMainBundle:Player');
		$this->mapRepository = $this->doctrine->getRepository('DotaFanMainBundle:GameMap');
		$this->banRepository = $this->doctrine->getRepository('DotaFanMainBundle:GameBan');
		$this->pickRepository = $this->doctrine->getRepository('DotaFanMainBundle:GamePick');

		$page = (int) $input->getArgument('page');
		if ($page < 1) {
			$page = 1;
		}

		$this->whole = (bool) $input->getArgument('whole');

		$finishedGames = 0;

		while (true) {

			if ($finishedGames == 0) {
				$this->log("---");
				$this->log("Updating LIVE games");
				$this->log("---");
				$this->updateLiveGames();
			}

			$this->log("---");
			$this->log("Page $page");
			$this->log("---");

			try {

				$gamesData = $this->parser->getGames($page);

			} catch (\Exception $e) {

				$this->log('ERROR: could not parse games. Cause: '.$e->getMessage());
				$this->log('Sleeping and retrying ...');
				sleep(5);

				continue;

			}

			$n = 0;
			$restart = false;
			while ($n < $gamesData->count()) {

				$gameData = $gamesData->offsetGet($n);

				try {

					$game = $this->gameRepository->findByUriOrCreate($gameData['uri']);
					$isNew = !$game->getId();

					$this->em->getConnection()->beginTransaction();
					$this->updateGame($game);
					$this->em->flush();
					$this->em->getConnection()->commit();

					if (!$isNew && $game->getFinished() && ++$finishedGames >= 10) {
						if (!$this->whole) {
							$restart = true;
							break;
						}
					}

				} catch (\Exception $e) {
					$this->dispatchGameException($e, $gameData['uri']);
				}

				$n++;
			}

			if ($restart) {

				$this->log($finishedGames.' finished games parsed');
				if ($this->whole) {

					$this->log('Restarting...');

					$finishedGames = 0;
					$page = 1;

					$this->em->getConnection()->close();
					sleep(60);
					$this->em->getConnection()->connect();

				} else {
					break; // we are done now
				}

			} else {
				$page++;
			}

		}
	}

	private function updateGame(Entity\Game $game)
	{
		$gameData = $this->parser->getGame($game->getUri());

		$msg = sprintf("Game [%s] - [%s]",
			$gameData['team1']['name'], $gameData['team2']['name']);
		if ($gameData['status']) {
			$msg .= ', '.$gameData['status'];
		}
		$this->log($msg);

		$tournament = $this->tournamentRepository->findByUriOrCreate($gameData['tournament']['uri']);
		$tournament->setName($gameData['tournament']['name']);
		if ($tournament->getId()) {
			$tournament->setLastMatchDate($this->gameRepository->getMaxDateByTournamentId($tournament->getId()));
		}
		$this->em->persist($tournament);

		$team1 = $this->teamRepository->findByUriOrCreate($gameData['team1']['uri']);
		if ($team1->getId()) {
			$team1->setLastMatchDate($this->gameRepository->getMaxDateByTeamId($team1->getId()));
		}

		$udate = $team1->getUpdateDate();
		if (is_null($udate) || (time() - $udate->getTimestamp()) >= $this->getTeamUpdateThreshold()) {
			$this->log('Team '.$gameData['team1']['name']);
			$this->updateTeam($team1);
		}

		$team2 = $this->teamRepository->findByUriOrCreate($gameData['team2']['uri']);
		if ($team2->getId()) {
			$team2->setLastMatchDate($this->gameRepository->getMaxDateByTeamId($team2->getId()));
		}

		$udate = $team2->getUpdateDate();
		if (is_null($udate) || (time() - $udate->getTimestamp()) >= $this->getTeamUpdateThreshold()) {
			$this->log('Team '.$gameData['team2']['name']);
			$this->updateTeam($team2);
		}

		$game->setBracket($gameData['bracket']);
		$game->setFormat($gameData['format']);

		$m = [];
		if (preg_match('/(\d).*(\d)/', $gameData['status'], $m)) {

			$game->setFinished(Entity\Game::FINISHED_RESULT);
			$game->setResult(sprintf('%d:%d', $m[1], $m[2]));

			if ($m[1] > $m[2]) {
				$game->setWinner($team1);
			} else if ($m[1] < $m[2]) {
				$game->setWinner($team2);
			}

			$team1->setRating($this->gameRepository->getWonMatchesCount($team1->getId()));
			$team2->setRating($this->gameRepository->getWonMatchesCount($team2->getId()));

		} else if (strpos($gameData['status'], 'cancel') !== false) {
			$game->setFinished(Entity\Game::FINISHED_CANCELED);
		} else if (strpos($gameData['status'], 'postp') !== false) {
			$game->setFinished(Entity\Game::FINISHED_POSTPONED);
		} else if (strpos($gameData['status'], 'tba') !== false) {
			$game->setFinished(Entity\Game::FINISHED_TBA);
		} else {
			$game->setFinished(Entity\Game::FINISHED_NOT_PLAYED);
		}

		if ($game->getFinished() !== Entity\Game::FINISHED_RESULT) {
			$game->setResult($gameData['status']);
		}

		$game->setTeam1($team1);
		$game->setTeam2($team2);
		$gameDate = new \DateTime();
		$gameDate->setTimestamp(strtotime($gameData['fullDate']));
		$game->setDate($gameDate);
		$game->setTournament($tournament);

		$this->updateMaps($game, $team1, $team2, $gameData['played']);

		$this->em->persist($team1);
		$this->em->persist($team2);
		$this->em->persist($game);
	}

	private function updateMaps(Entity\Game $game, Entity\Team $team1, Entity\Team $team2, array $played)
	{
		foreach ($played as $i => $mapData) {

			$n = $i + 1;

			$map = $this->mapRepository->findOneBy([
				'game' => $game,
				'n' => $n
			]);

			if (!$map) {
				$map = new Entity\GameMap();
			}

			$map->setGame($game);
			$map->setN($n);
			$map->setResult($mapData['result']);

			$this->updateBans($team1, $map, $mapData['bans1']);
			$this->updateBans($team2, $map, $mapData['bans2']);
			$this->updatePicks($team1, $map, $mapData['picks1'], $mapData['players1']);
			$this->updatePicks($team2, $map, $mapData['picks2'], $mapData['players2']);

			$this->em->persist($map);
		}

	}

	private function updateBans(Entity\Team $team, Entity\GameMap $map, array $bans)
	{
		foreach ($bans as $n => $heroName) {

			$ban = $this->banRepository->findOneBy([
				'n' => $n,
				'team' => $team,
				'map' => $map
			]);

			if (!$ban) {
				$ban = new Entity\GameBan();
			}

			$ban->setN($n);
			$ban->setTeam($team);
			$ban->setMap($map);
			$ban->setHero($heroName);

			$this->em->persist($ban);
		}
	}

	private function updatePicks(Entity\Team $team, Entity\GameMap $map, array $heroes, array $players)
	{
		for ($n = 0; $n < 5; $n++) {

			$pick = $this->pickRepository->findOneBy([
				'n' => $n,
				'team' => $team,
				'map' => $map
			]);

			if (!$pick) {
				$pick = new Entity\GamePick();
			}

			$pick->setN($n);
			$pick->setMap($map);
			$pick->setTeam($team);

			if (isset($heroes[$n])) {
				$heroName = $heroes[$n];
				$pick->setHero($heroName);
			}

			if (isset($players[$n])) {
				$playerData = $players[$n];
				$player = $this->playerRepository->findOneByUri($playerData['uri']);
				if ($player) {
					$pick->setPlayer($player);
				}
			}

			$this->em->persist($pick);
		}
	}

	private function updateTeam(Entity\Team $team)
	{
		$teamData = $this->parser->getTeam($team->getUri());

		$team->setName($teamData['fullName']);
		$team->setShortName($teamData['shortName']);
		$team->setCountry($teamData['from']);
		$team->setUri($teamData['uri']);
		$team->setUpdateDate(new \DateTime());

		foreach ($team->getPlayers() as $player) {
			/** @var Entity\Player $player */
			$player->setTeam(null);

			$this->em->persist($player);
		}

		foreach ($teamData['roster'] as $playerData) {
			$player = $this->playerRepository->findByUriOrCreate($playerData['uri']);

			$player->setCountry($playerData['from']);
			$player->setName($playerData['name']);
			$player->setNickname($playerData['nickname']);
			$player->setRole($playerData['role']);
			$player->setTeam($team);

			$this->em->persist($player);
		}

		$this->em->persist($team);

	}

	private function log($message)
	{
		echo date('Y-m-d H:i:s - ') . $message . "\n";
	}

	private function logToFile($message)
	{
		$dir = $this->getContainer()->getParameter('logs_dir');
		$path = $dir . 'jd_loader.log';
		$text = date('Y-m-d H:i:s - ') . $message . "\n";
		file_put_contents($path, $text, FILE_APPEND);
	}

	private function updateLiveGames()
	{
		$games = $this->gameRepository->findLiveGames();
		foreach ($games as $game) {
			/** @var Entity\Game $game */
			try {
				$this->em->getConnection()->beginTransaction();
				$this->updateGame($game);
				$this->em->flush();
				$this->em->getConnection()->commit();
			} catch (\Exception $e) {
				$this->dispatchGameException($e, $game->getUri());
			}
		}
	}

	private function dispatchGameException(\Exception $e, $uri)
	{
		$this->log(sprintf('EXCEPTION: %s, %s', get_class($e), $e->getMessage()));
		$this->logToFile(sprintf("Failed game [%s]", $uri));

		$this->em->clear();

		$this->em->getConnection()->close();
		$this->em->getConnection()->connect();

		if (!$this->em->isOpen()) {
			$this->em = $this->em->create(
				$this->em->getConnection(),
				$this->em->getConfiguration()
			);
		}

		if ($e instanceof GameNotFoundException) {
			$game = $this->gameRepository->findOneByUri($e->getUri());
			if ($game) {

				$this->em->getConnection()->beginTransaction();
				$this->em->remove($game);
				$this->em->flush();
				$this->em->getConnection()->commit();

			}
		}
	}

	private function getTeamUpdateThreshold()
	{
		return $this->getContainer()->getParameter('team_update_threshold');
	}
}

