<?php

namespace DotaFan\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use DotaFan\ApiBundle\Repository\ApiRepository;
use Symfony\Component\HttpFoundation\Request;

class TeamController extends Controller
{
	/** @return ApiRepository */
	private function getApiRepository()
	{
		return $this->get('api.repository');
	}

	private function dispatchException(\Exception $e)
	{
		return new JsonResponse([
			'error' => [
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			]
		]);
	}

	public function infoAction($id, Request $request)
	{
		try {

			$repository = $this->getApiRepository();
			$ret = [
				'team' => $repository->getTeamInfo($id)
			];

			if ($token = $request->get('token')) {
				$ret['isFollowing'] = $repository->isFollowingTeam($id, $token);
			} else {
				$ret['isFollowing'] = false;
			}

			return new JsonResponse($ret);

		} catch (\Exception $e) {
			return $this->dispatchException($e);
		}
	}

	public function followAction(Request $request)
	{
		try {

			$id = $request->get('id');
			$token = $request->get('token');

			$em = $this->getDoctrine()->getManager();
			/** @var \Doctrine\DBAL\Connection $conn */
			$conn = $em->getConnection();

			$conn->beginTransaction();
			$this->getApiRepository()->followTeam($id, $token);
			$em->flush();
			$conn->commit();

			return new JsonResponse(['ok' => true]);

		} catch (\Exception $e) {

			if ($conn->isTransactionActive()) {
				$conn->rollBack();
			}

			return $this->dispatchException($e);
		}
	}

	public function unfollowAction(Request $request)
	{
		try {

			$id = $request->get('id');
			$token = $request->get('token');

			$em = $this->getDoctrine()->getManager();
			/** @var \Doctrine\DBAL\Connection $conn */
			$conn = $em->getConnection();

			$conn->beginTransaction();
			$this->getApiRepository()->unfollowTeam($id, $token);
			$em->flush();
			$conn->commit();

			return new JsonResponse(['ok' => true]);

		} catch (\Exception $e) {

			if ($conn->isTransactionActive()) {
				$conn->rollBack();
			}

			return $this->dispatchException($e);
		}
	}

	public function listAction(Request $request, $page = 1)
	{
		try {

			/*$this
				->get('doctrine')
				->getConnection()
				->getConfiguration()
				->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());*/

			$repository = $this->getApiRepository();
			$teams = $repository->getTeams($page, $request->get('name'));

			return new JsonResponse([
				'teams' => $repository->teamsToArray($teams)
			]);

		} catch (\Exception $e) {
			return $this->dispatchException($e);
		}
	}
}
