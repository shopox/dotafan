<?php

namespace DotaFan\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use DotaFan\ApiBundle\Repository\ApiRepository;
use Symfony\Component\HttpFoundation\Request;

class MatchController extends Controller
{
	/** @return ApiRepository */
	private function getApiRepository()
	{
		return $this->get('api.repository');
	}

	/** @return JsonResponse */
	private function matchesResponse(array $matches)
	{
		return new JsonResponse([
			'matches' => $matches
		]);
	}

	private function dispatchException(\Exception $e)
	{
		return new JsonResponse([
			'error' => [
				'code' => $e->getCode(),
				'message' => $e->getMessage()
			]
		]);
	}

	public function listAction(Request $request, $page)
	{
		$filter = [];

		if ($tournamentId = $request->get('tournament_id')) {
			$filter['tournament_id'] = $tournamentId;
		}

		if ($teamId = $request->get('team_id')) {
			$filter['team_id'] = $teamId;
		}

		$repository = $this->getApiRepository();

		$games = $repository->findGames($page, $filter);

		return $this->matchesResponse(
			$repository->gamesToArray($games)
		);
	}

    public function listUpcomingAction()
    {
		$repository = $this->getApiRepository();
		$games = $repository->getUpcomingGames();

		return $this->matchesResponse($repository->gamesToArray($games));
    }

	public function listPastAction($page)
	{

		/*$this
			->get('doctrine')
			->getConnection()
			->getConfiguration()
			->setSQLLogger(new \Doctrine\DBAL\Logging\EchoSQLLogger());*/

		$repository = $this->getApiRepository();
		$games = $repository->getPastGames($page);

		return $this->matchesResponse($repository->gamesToArray($games));
	}

	public function listLiveAction()
	{
		$repository = $this->getApiRepository();
		$games = $repository->getLiveGames();

		return $this->matchesResponse($repository->gamesToArray($games));
	}

	public function infoAction(Request $request, $id)
	{
		try {

			$repository = $this->getApiRepository();
			$ret = [
				'match' => $repository->getGameInfo($id)
			];

			if ($token = $request->get('token')) {
				$ret['isFollowing'] = $repository->isFollowingGame($id, $token);
			} else {
				$ret['isFollowing'] = false;
			}

			return new JsonResponse($ret);

		} catch (\Exception $e) {
			return $this->dispatchException($e);
		}

	}

	public function followAction(Request $request)
	{
		try {

			$id = $request->get('id');
			$token = $request->get('token');

			$em = $this->getDoctrine()->getManager();
			/** @var \Doctrine\DBAL\Connection $conn */
			$conn = $em->getConnection();

			$conn->beginTransaction();
			$this->getApiRepository()->followGame($id, $token);
			$em->flush();
			$conn->commit();

			return new JsonResponse(['ok' => true]);

		} catch (\Exception $e) {

			if ($conn->isTransactionActive()) {
				$conn->rollBack();
			}

			return $this->dispatchException($e);
		}
	}

	public function unfollowAction(Request $request)
	{
		try {

			$id = $request->get('id');
			$token = $request->get('token');

			$em = $this->getDoctrine()->getManager();
			/** @var \Doctrine\DBAL\Connection $conn */
			$conn = $em->getConnection();

			$conn->beginTransaction();
			$this->getApiRepository()->unfollowGame($id, $token);
			$em->flush();
			$conn->commit();

			return new JsonResponse(['ok' => true]);

		} catch (\Exception $e) {

			if ($conn->isTransactionActive()) {
				$conn->rollBack();
			}

			return $this->dispatchException($e);
		}
	}
}
