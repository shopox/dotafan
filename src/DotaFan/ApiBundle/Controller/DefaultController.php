<?php

namespace DotaFan\ApiBundle\Controller;

use DotaFan\MainBundle\Entity\Game;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('DotaFanApiBundle:Default:index.html.twig', array('name' => $name));
    }
}
