<?php
namespace DotaFan\ApiBundle\Repository;

use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\QueryBuilder;
use DotaFan\ApiBundle\Repository\Exception\AlreadySubscribedException;
use DotaFan\ApiBundle\Repository\Exception\DeviceTokenInvalidException;
use DotaFan\ApiBundle\Repository\Exception\NotSubscribedException;
use DotaFan\MainBundle\Entity\Game;
use DotaFan\MainBundle\Entity\GameBan;
use DotaFan\MainBundle\Entity\GameMap;
use DotaFan\MainBundle\Entity\GamePick;
use DotaFan\MainBundle\Entity\Subscription;
use DotaFan\MainBundle\Entity\SubscriptionRepository;
use DotaFan\MainBundle\Entity\Team;
use DotaFan\MainBundle\Entity\Player;
use DotaFan\MainBundle\Entity\GameRepository;
use DotaFan\MainBundle\Entity\PlayerRepository;
use DotaFan\MainBundle\Entity\TeamRepository;
use Doctrine\ORM\EntityManager;
use DotaFan\MainBundle\Entity\Tournament;
use DotaFan\MainBundle\Entity\TournamentRepository;
use DotaFan\MainBundle\Entity\User;
use DotaFan\MainBundle\Entity\UserRepository;
use Symfony\Component\Config\Definition\Exception\Exception;

class ApiRepository
{
	const PAGE_SIZE = 50;

	private $em;

	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

	/** @return GameRepository */
	private function getGameRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:Game');
	}

	/** @return TeamRepository */
	private function getTeamRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:Team');
	}

	/** @return PlayerRepository */
	private function getPlayerRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:Player');
	}

	/** @return UserRepository */
	private function getUserRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:User');
	}

	/** @return SubscriptionRepository */
	private function getSubscriptionRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:Subscription');
	}

	/** @return TournamentRepository */
	private function getTournamentRepository()
	{
		return $this->em->getRepository('DotaFanMainBundle:Tournament');
	}

	private function getGamesQueryBuilder()
	{
		$builder = $this->getGameRepository()->createQueryBuilder('g');

		$builder->innerJoin('g.team1', 't1');
		$builder->addSelect('t1');

		$builder->innerJoin('g.team2', 't2');
		$builder->addSelect('t2');

		$builder->innerJoin('g.tournament', 't');
		$builder->addSelect('t');

		return $builder;
	}

	public function findGames($page = 1, $filter = [])
	{
		$builder = $this->getGamesQueryBuilder();

		if ($filter['tournament_id']) {
			$builder->andWhere('g.tournamentId = :tournament_id');
			$builder->setParameter('tournament_id', (int) $filter['tournament_id']);
		}

		if ($filter['team_id']) {
			$builder->andWhere('(g.team1Id = :team_id OR g.team2Id = :team_id)');
			$builder->setParameter('team_id', (int) $filter['team_id']);
		}

		$builder->orderBy('g.date', 'DESC');
		$this->setPage($builder, $page);

		return $builder->getQuery()->getResult();
	}

	public function getLiveGames()
	{
		$builder = $this->getGamesQueryBuilder();
		$builder->where('g.finished IN (:finished) AND g.date <= :date');
		$builder->setParameter('finished', [Game::FINISHED_NOT_PLAYED]);
		$builder->setParameter('date', new \DateTime());
		$builder->orderBy('g.date', 'DESC');

		return $builder->getQuery()->getResult();
	}

	public function getPastGames($page = 1)
	{
		$builder = $this->getGamesQueryBuilder();
		$builder->where('g.finished IN (:finished) AND g.date <= :date');
		$builder->setParameter('finished', [
			Game::FINISHED_RESULT,

			Game::FINISHED_CANCELED,
			Game::FINISHED_POSTPONED,
			Game::FINISHED_TBA
		]);
		$builder->setParameter('date', new \DateTime());
		$builder->orderBy('g.date', 'DESC');

		$this->setPage($builder, $page);

		return $builder->getQuery()->getResult();
	}

	public function getUpcomingGames()
	{
		$builder = $this->getGamesQueryBuilder();
		$builder->where('g.finished IN (:finished) AND g.date > :date');
		$builder->setParameter('finished', [
			Game::FINISHED_NOT_PLAYED,

			Game::FINISHED_CANCELED,
			Game::FINISHED_POSTPONED,
			Game::FINISHED_TBA
		]);
		$builder->setParameter('date', new \DateTime());
		$builder->orderBy('g.date', 'ASC');

		return $builder->getQuery()->getResult();
	}

	/** @return QueryBuilder */
	private function setPage(QueryBuilder $builder, $page, $limit = self::PAGE_SIZE)
	{
		$page = (int) $page;
		if ($page < 1) {
			$page = 1;
		}
		$limit = (int) $limit;
		$offset = ($page - 1) * $limit;
		$builder->setMaxResults($limit);
		$builder->setFirstResult($offset);
	}

	/** @return array */
	public function gamesToArray(array $games)
	{
		$ret = [];

		foreach ($games as $game) {
			/** @var Game $game */
			$ret[] = $this->gameToHash($game, true);
		}

		return $ret;
	}

	private function formatDate(\DateTime $date)
	{
		return $date->format(\DateTime::ISO8601);
	}

	/** @return array */
	public function gameToHash(Game $game, $reduced = false)
	{
		$ret = [
			'id' => $game->getId(),
//			'bracket' => $game->getBracket(),
			'date' => $this->formatDate($game->getDate()),
			'finished' => $game->getFinished(),
			'result' => $game->getResult(),
			'team1' => $this->teamToHash($game->getTeam1(), false, $reduced),
			'team2' => $this->teamToHash($game->getTeam2(), false, $reduced),
//			'tournament' => $this->tournamentToHash($game->getTournament())
		];

		if ($reduced) {

		} else {
			$ret['bracket'] = $game->getBracket();
			$ret['tournament'] = $this->tournamentToHash($game->getTournament());
			$ret['format'] = $game->getFormat();
		}

		return $ret;
	}

	public function getGameInfo($id)
	{
		$game = $this->getGameRepository()->find($id);
		if ($game) {
			/** @var Game $game */

			$ret = $this->gameToHash($game);

			$maps = [];
			foreach ($game->getMaps() as $map) {
				/** @var GameMap $map */
				$mapData = [
					'n' => $map->getN(),
					'result' => $map->getResult()
				];

				foreach ($map->getBans() as $ban) {
					/** @var GameBan $ban */
					$f = 'bans'. ($ban->getTeamId() == $game->getTeam1Id() ? '1' : '2');
					$mapData[$f][] = [
						'n' => $ban->getN(),
						'hero' => $ban->getHero()
					];
				}

				foreach ($map->getPicks() as $pick) {
					/** @var GamePick $pick */
					$f = 'picks'. ($pick->getTeamId() == $game->getTeam1Id() ? '1' : '2');
					$mapData[$f][] = [
						'n' => $pick->getN(),
						'hero' => $pick->getHero(),
						'nickname' => ($player = $pick->getPlayer()) ? $player->getNickname() : ''
					];
				}

				$maps[] = $mapData;
			}

			$ret['maps'] = $maps;

			return $ret;
		}

		return null;
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws AlreadySubscribedException
	 */
	public function followGame($gameId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$game = $this->getGameRepository()->find($gameId);
		if (!$game) {
			throw new EntityNotFoundException('Game not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'gameId' => $gameId
		]);
		if ($subscription) {
			throw new AlreadySubscribedException('Already subscribed');
		}

		$subscription = new Subscription();
		$subscription->setGame($game);
		$subscription->setUser($user);

		$this->em->persist($subscription);
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws NotSubscribedException
	 */
	public function unfollowGame($gameId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$game = $this->getGameRepository()->find($gameId);
		if (!$game) {
			throw new EntityNotFoundException('Tournament not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'gameId' => $gameId
		]);
		if (!$subscription) {
			throw new NotSubscribedException('Not subscribed');
		}

		$this->em->remove($subscription);
	}

	/** @return bool */
	public function isFollowingGame($gameId, $deviceToken, $exact = false)
	{
		$game = $this->getGameRepository()->find($gameId);
		if (!$game) {
			throw new EntityNotFoundException('Game not found');
		}

		/** @var User $user */
		$user = $this->getUserRepository()->findOneByDeviceToken($deviceToken);
		if (!$user) {
			return false;
		}

		if ($exact) {

			$subscription = $this->getSubscriptionRepository()->findOneBy([
				'userId' => $user->getId(),
				'gameId' => $gameId
			]);

			return !empty($subscription);

		} else {
			$list = $this->getUserRepository()->getGameSubscribers($gameId, $user->getId());
			return !empty($list);
		}
	}

	/** @return array */
	public function getTeamInfo($id, $includeRoster = true)
	{
		$team = $this->getTeamRepository()->find($id);
		if ($team) {
			/** @var Team $team */
			return $this->teamToHash($team, $includeRoster);
		} else {
			throw new EntityNotFoundException('Team not found');
		}
	}

	/** @return array */
	private function teamToHash(Team $team, $includeRoster = true, $reduced = false)
	{
		$ret = [
			'id' => $team->getId(),
//			'name' => $team->getName(),
			'shortName' => $team->getShortName(),
			'country' => $team->getCountry(),
		];

		if ($reduced) {

		} else {
			$ret['name'] = $team->getName();
			$ret['logoUrl'] = $team->getLogoUrl();
		}

		if ($includeRoster) {
			$ret['roster'] = $this->getTeamRoster($team->getId());
		}

		return $ret;
	}

	/** @return array */
	private function getTeamRoster($teamId)
	{
		$players = $this->getPlayerRepository()->findByTeamId($teamId);
		$ret = [];
		foreach ($players as $player) {
			/** @var Player $player */
			$ret[] = [
				'name' => $player->getName(),
				'nickname' => $player->getNickname(),
				'country' => $player->getCountry(),
				'role' => $player->getRole()
			];
		}

		return $ret;
	}

	/** @return User */
	private function checkUsersToken($deviceToken)
	{
		$user = $this->getUserRepository()->findOneByDeviceToken($deviceToken);
		if (!$user) {

			$user = new User();
			$user->setDeviceToken($deviceToken);
			if (!$user->isDeviceTokenValid()) {
				throw new DeviceTokenInvalidException('Device token is invalid');
			}

			$this->em->persist($user);
		}

		return $user;
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws AlreadySubscribedException
	 */
	public function followTeam($teamId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$team = $this->getTeamRepository()->find($teamId);
		if (!$team) {
			throw new EntityNotFoundException('Team not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'teamId' => $teamId
		]);
		if ($subscription) {
			throw new AlreadySubscribedException('Already subscribed');
		}

		$subscription = new Subscription();
		$subscription->setTeam($team);
		$subscription->setUser($user);

		$this->em->persist($subscription);
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws NotSubscribedException
	 */
	public function unfollowTeam($teamId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$team = $this->getTeamRepository()->find($teamId);
		if (!$team) {
			throw new EntityNotFoundException('Team not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'teamId' => $teamId
		]);
		if (!$subscription) {
			throw new NotSubscribedException('Not subscribed');
		}

		$this->em->remove($subscription);
	}

	/**
	 * @throws EntityNotFoundException
	 * @return bool
	 */
	public function isFollowingTeam($teamId, $deviceToken)
	{
		$team = $this->getTeamRepository()->find($teamId);
		if (!$team) {
			throw new EntityNotFoundException('Team not found');
		}

		/** @var User $user */
		$user = $this->getUserRepository()->findOneByDeviceToken($deviceToken);
		if (!$user) {
			return false;
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'teamId' => $teamId
		]);

		if ($subscription) {
			return true;
		} else {
			return false;
		}
	}

	/** @return array */
	public function getTournamentInfo($id)
	{
		$tournament = $this->getTournamentRepository()->find($id);
		if ($tournament) {
			/** @var Tournament $tournament */
			return $this->tournamentToHash($tournament);
		} else {
			throw new EntityNotFoundException('Tournament not found');
		}
	}

	private function tournamentToHash(Tournament $tournament)
	{
		return [
			'id' => $tournament->getId(),
			'name' => $tournament->getName()
		];
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws AlreadySubscribedException
	 */
	public function followTournament($tournamentId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$tournament = $this->getTournamentRepository()->find($tournamentId);
		if (!$tournament) {
			throw new EntityNotFoundException('Tournament not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'tournamentId' => $tournamentId
		]);
		if ($subscription) {
			throw new AlreadySubscribedException('Already subscribed');
		}

		$subscription = new Subscription();
		$subscription->setTournament($tournament);
		$subscription->setUser($user);

		$this->em->persist($subscription);
	}

	/**
	 * @throws DeviceTokenInvalidException
	 * @throws EntityNotFoundException
	 * @throws NotSubscribedException
	 */
	public function unfollowTournament($tournamentId, $deviceToken)
	{
		$user = $this->checkUsersToken($deviceToken);

		$tournament = $this->getTournamentRepository()->find($tournamentId);
		if (!$tournament) {
			throw new EntityNotFoundException('Tournament not found');
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'tournamentId' => $tournamentId
		]);
		if (!$subscription) {
			throw new NotSubscribedException('Not subscribed');
		}

		$this->em->remove($subscription);
	}

	/**
	 * @throws EntityNotFoundException
	 * @return bool
	 */
	public function isFollowingTournament($tournamentId, $deviceToken)
	{
		$tournament = $this->getTournamentRepository()->find($tournamentId);
		if (!$tournament) {
			throw new EntityNotFoundException('Tournament not found');
		}

		/** @var User $user */
		$user = $this->getUserRepository()->findOneByDeviceToken($deviceToken);
		if (!$user) {
			return false;
		}

		$subscription = $this->getSubscriptionRepository()->findOneBy([
			'userId' => $user->getId(),
			'tournamentId' => $tournamentId
		]);

		if ($subscription) {
			return true;
		} else {
			return false;
		}
	}

	/** @return array */
	public function getTeams($page = 1, $name = null)
	{
		$builder = $this->getTeamRepository()->createQueryBuilder('t');

		if (is_string($name)) {
			$p = preg_split('/[\s]+/', trim($name));
			$builder->andWhere('(UPPER(t.name) LIKE UPPER(:name) OR UPPER(t.shortName) LIKE UPPER(:name))');
			$builder->setParameter('name', '%'. join('%', $p) .'%');
		}

		$builder->orderBy('t.rating', 'DESC');
		$this->setPage($builder, $page);

		return $builder->getQuery()->getResult();
	}

	public function getTournaments($page = 1, $name = null)
	{
		$builder = $this->getTournamentRepository()->createQueryBuilder('t');

		if (is_string($name)) {
			$p = preg_split('/[\s]+/', trim($name));
			$builder->andWhere('(UPPER(t.name) LIKE UPPER(:name))');
			$builder->setParameter('name', '%'. join('%', $p) .'%');
		}

		$builder->orderBy('t.last_match_date', 'DESC');
		$this->setPage($builder, $page);

		return $builder->getQuery()->getResult();
	}

	public function tournamentsToArray(array $tournaments)
	{
		$ret = [];
		foreach ($tournaments as $tournament) {
			$ret[] = $this->tournamentToHash($tournament);
		}

		return $ret;
	}

	public function teamsToArray(array $teams)
	{
		$ret = [];
		foreach ($teams as $team) {
			$ret[] = $this->teamToHash($team, false);
		}

		return $ret;
	}
}